﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RibosomeController : MonoBehaviour {

    public Transform mRNAExit;
    public Transform proteinAnchor;

    public TranslationGroup[] translationGroups;
    public Molecule mRNAmolecule;


    private MoleculeManager _molManager;
    private mRNAController _mRNAcontroller;
    private float normalizedLength = 0f;
    private List<float> spawnLengths;
    private int proteinIndex = 0;
   
    private void Awake()
    {
        _molManager = FindObjectOfType(typeof(MoleculeManager)) as MoleculeManager;

    }

    public void StartTranslation(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule)
    {
        StartCoroutine(C_Translation(moleculeStateMachine, boundMolecule));
    }

    private IEnumerator C_Translation(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule)
    {
        mRNAHandleController handle = moleculeStateMachine.GetComponentInChildren<mRNAHandleController>();
        if(handle == null)
        {
            yield break;
        }
        mRNAController controller = handle.mRNAController;
        float startLength = controller.Length();
        float minLength = controller.minLength;
        normalizedLength = 0f;
        proteinIndex = 0;

        //get proteins to spawn
        TranslationGroup group = GetTranslationGroup(mRNAmolecule);
        SetSpawnLengths(group.proteins.Length);

        //flag as growing
        controller.isGrowing = true;
        

        while(_mRNAcontroller != null && controller.Length() > minLength)
        {
            controller.Shrink(); //shrink incomming
            _mRNAcontroller.Grow(); //gorw outgoing
            normalizedLength = controller.Length().Remap(startLength, minLength, 0f, 1f);
            CheckLength(group);
            yield return null;
        }

        controller.Deactive();
        controller.isGrowing = false;
        SpawnmRNA();
        yield return new WaitForSeconds(1f);
        Destroy(boundMolecule.gameObject);
        EndTranslation();
        
    }

    private void SetSpawnLengths(float proteinCount)
    {
        spawnLengths = new List<float>();
        float checkPoint = 0f;
        float interval = 1f / proteinCount;
        for(int i = 0; i < proteinCount; i++)
        {
            checkPoint += interval;
            spawnLengths.Add(checkPoint);
        }
    }

    private void CheckLength(TranslationGroup group)
    {
        if(normalizedLength >= spawnLengths[proteinIndex])
        {
            //spawn molecule
            _molManager.SpawnMolecule(group.proteins[proteinIndex], proteinAnchor);
            if (proteinIndex < spawnLengths.Count - 1)
                proteinIndex++;
        }
    }

    private void EndTranslation()
    {
        GetComponent<MoleculeStateMachine>().ResetMolecule(2f);
    }

    //growth
    public void SetmRNA(mRNAController mRNA, Molecule mol)
    {
        _mRNAcontroller = mRNA;
        _mRNAcontroller.isGrowing = true;
        mRNAmolecule = mol;
    }

    private void SpawnmRNA()
    {
        MoleculeStateMachine newMolStateMachine = _molManager.SpawnMolecule(mRNAmolecule, mRNAExit);

        if (_mRNAcontroller == null)
            return;
        _mRNAcontroller.handle.transform.SetParent(newMolStateMachine.modelParent, false);
        _mRNAcontroller.isGrowing = false;
        _mRNAcontroller = null;

        newMolStateMachine.transform.SetParent(null, false);

        newMolStateMachine.GetComponent<Rigidbody>().AddForce(newMolStateMachine.transform.forward * -100f);
       
    }

    private TranslationGroup GetTranslationGroup(Molecule mRNA)
    {
        foreach(TranslationGroup g in translationGroups)
        {
            if (g.mRNA == mRNA)
                return g;
        }
        return null;
    }
}
