﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class mRNAController : MonoBehaviour {

    public ObiRopeCursor cursor;
    public ObiRope rope;
    public float minLength = 0.1f;
    public mRNAHandleController handle;
    private mRNAObjectPooler _objectPooler;
    public bool isGrowing;

    // Use this for initialization
    void Start()
    {
        cursor = GetComponentInChildren<ObiRopeCursor>();
        rope = cursor.GetComponent<ObiRope>();
        cursor.ChangeLength(minLength);
    }


    public void Grow()
    {
        cursor.ChangeLength(rope.RestLength + 0.5f * Time.deltaTime);
    }

    public void Shrink()
    {
        cursor.ChangeLength(rope.RestLength - 0.5f * Time.deltaTime);
    }

    public float Length()
    {
        return rope.RestLength;
    }

    public void Activate(mRNAObjectPooler pooler)
    {
        _objectPooler = pooler;
        cursor.ChangeLength(minLength);
    }

    public void Deactive()
    {
        _objectPooler.DeactivatemRNA(this);
    }

    public bool ActiveAndAvailable()
    {
        if (isGrowing)
            return false;
        MoleculeStateMachine moleculeStateMachine = handle.GetComponentInParent<MoleculeStateMachine>();
        if (moleculeStateMachine == null)
            return false;
        if (moleculeStateMachine.currentState == MoleculeManager.Instance.freeState)
            return true;
        return false;

    }
}
