﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LabelController : MonoBehaviour {

    private Vector3 origScale;
    private CanvasGroup canvasGroup;
    public TMP_Text labelText;


    private void Awake()
    {
        origScale = transform.localScale;
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void SetLabel(string text)
    {
        labelText.text = text;
        labelText.rectTransform.sizeDelta = labelText.GetPreferredValues(text);
    }

    public void ToggleLabel(bool show)
    {
        if (show)
            canvasGroup.alpha = 1f;
        else
            canvasGroup.alpha = 0f;
    }

    private void LateUpdate()
    {
        FaceHMD();
    }

    public void FaceHMD()
    {
        Camera camera = Camera.main;
        transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward, Vector3.up);

        float dist = Vector3.Distance(transform.position, camera.transform.position);
        transform.localScale = origScale * dist;
    }
}
