﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Actions/Pointer/Show Label")]
public class ShowLabelAction : StateAction {

    public bool show;

    public override void Act(StateMachine stateMachine)
    {
        PointerStateMachine pointerStateMachine = stateMachine as PointerStateMachine;

        if (pointerStateMachine == null)
            return;

        //are we pointing at Molecule?
        MoleculeStateMachine molecule = pointerStateMachine.PointerEventArgs.target.GetComponent<MoleculeStateMachine>();

        //if so, show the label at hit point
        if (molecule != null)
            molecule.ToggleLabel(show, pointerStateMachine.PointerEventArgs.hitPostion);
    }
}
