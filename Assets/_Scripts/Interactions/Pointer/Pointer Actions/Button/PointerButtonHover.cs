﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[CreateAssetMenu(menuName = "Actions/Pointer/Button Hover")]
public class PointerButtonHover : StateAction {

    public bool hoverIn;

    public override void Act(StateMachine stateMachine)
    {
        PointerStateMachine pointerStateMachine = stateMachine as PointerStateMachine;

        if (pointerStateMachine == null)
            return;

        //are we pointing at a UIElement
        UIElement button = pointerStateMachine.PointerEventArgs.target.GetComponent<UIElement>();

        Hand hand = pointerStateMachine.Pointer.Hand;


        if (button != null)
        {
            if(hoverIn)
            {
                InputModule.instance.HoverBegin(button.gameObject);
                ControllerButtonHints.ShowButtonHint(hand, hand.uiInteractAction);
            } 
            else
            {
                InputModule.instance.HoverEnd(button.gameObject);
                ControllerButtonHints.HideButtonHint(hand, hand.uiInteractAction);
            }    
        }
    }
}
