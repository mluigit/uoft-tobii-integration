﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[CreateAssetMenu(menuName = "Actions/Pointer/Button Click")]
public class PointerButtonAction : StateAction {

    public override void Act(StateMachine stateMachine)
    {
        PointerStateMachine pointerStateMachine = stateMachine as PointerStateMachine;

        if (pointerStateMachine == null)
            return;

        //are we pointing at a UIElement
        UIElement button = pointerStateMachine.PointerEventArgs.target.GetComponent<UIElement>();

        Hand hand = pointerStateMachine.Pointer.Hand;

        if (button != null)
        {
            InputModule.instance.Submit(button.gameObject);
            ControllerButtonHints.HideButtonHint(hand, hand.uiInteractAction);
            //button.onHandClick.Invoke(hand);
        }
    }
}
