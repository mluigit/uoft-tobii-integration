﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TobiiProVRAnalytics;

public class TobiiReset : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject tobii = Object.FindObjectOfType<TobiiProVRAnalyticsManager>().gameObject;
        if (tobii != null)
        {
            tobii.SetActive(false);
            StartCoroutine(ResetTobii());
            tobii.SetActive(true);
        }
	}
	
	// Update is called once per frame
	private IEnumerator ResetTobii()
    {
        yield return new WaitForSeconds(0.5f);
    }
}
