// Copyright (c) 2018 AXS Biomedical Animation Studio Inc.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AXSCore;

public class AnalyticsTracker : MonoBehaviour
{
    //ADD METRICS AS NEEDED.
    public enum Metric
    {
        StartTime,
        EndTime,
        Timeout,
        button_ee_miras_mother,
        button_ee_middle_child,
        button_ee_house_plants,
        ExitPoint,
        LastButton,
        References
    }

    private static AnalyticsTracker _instance;
    public static AnalyticsTracker Instance { get { return _instance; } }

    public string userID;
    //public Metric startMetric;
    //public Metric endMetric;
    public string FILE_NAME = "_analytics.xml";
    public string DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public bool debug;



    public class AnalyticsItem
    {
        /*
        [XmlAttribute]
        public string name;
        [XmlAttribute]
        public string value;
        */
        public string id;
        public string timestamp;
        public string objectName;
        public string interaction;
    }

    [Serializable]
    public class AnalyticsSession
    {
        public List<AnalyticsItem> items = new List<AnalyticsItem>();
    }

    //BACK UP VARIABLES
    [HideInInspector]
    public List<AnalyticsSession> sessions;
    [HideInInspector]
    public AnalyticsSession currentSession;
    [HideInInspector]
    public bool isReady;


    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        sessions = new List<AnalyticsSession>();
        currentSession = new AnalyticsSession();
        LoadSessions();
    }


    
    void OnApplicationQuit()
    {
        if (currentSession.items.Count > 0)
        {
            Application.CancelQuit();
            LogItem(DateTime.Now, "Application", "End Session");
            SaveSession();
            Application.Quit();
        }
    }

    //ADDS PREVIOUS ENTRIES TO LIST OF ANALYTICS SESSIONS
    private void LoadSessions()
    {
        StartCoroutine(WaitForFile());
    }

    IEnumerator WaitForFile()
    {
        isReady = false;

        while (!ValidateFile())
            yield return null;

        sessions = GenericDeSerialize<AnalyticsSession>();
        isReady = true;
    }

    //STARTS NEW SESSION
    public void NewSession()
    {
        if (currentSession.items.Count > 0)
            SaveSession();

        LoadSessions();
        currentSession = new AnalyticsSession();

        //log date and time of start of session
        //LogMetric(Metric.StartTime, DateTime.Now);
    }

    //ENDS CURRENT SESSION AND SAVES DATA TO XML FILE
    public void SaveSession(bool timeout)
    {
        //LogMetric(Metric.Timeout, timeout);
        SaveSession();
    }

    public void SaveSession()
    {
        if (currentSession.items.Count < 1)
            return;

        //log date and time of end of session
        //LogMetric(Metric.EndTime, DateTime.Now);
        

        sessions.Add(currentSession);
        Serialize<AnalyticsSession>(sessions);
    }

    /*
    //LOG CUSTOM EVENT
    public void LogMetric<T>(Metric name, T value)
    {
        string valueString = "";
        if (value.GetType() == typeof(DateTime))
        {
            T genericDate = (T)(object)value;
            DateTime date = (DateTime)(object)genericDate;
            valueString = date.ToString(DATETIME_FORMAT);
        }
        else
            valueString = value.ToString();
        LogItem(name.ToString(), valueString);
    }

    //LOG CUSTOM EVENT USING STATE
    public void LogMetric<T>(State name, T value)
    {
        string valueString = "";
        if (value.GetType() == typeof(DateTime))
        {
            T genericDate = (T)(object)value;
            DateTime date = (DateTime)(object)genericDate;
            valueString = date.ToString(DATETIME_FORMAT);
        }
        else
            valueString = value.ToString();
        LogItem(name.ToString(), valueString);
    }
    */
    //LOG ITEMS
    //private void LogItem(string name, string value)
    public void LogItem(DateTime _timestamp, string _objectName, string _interaction)
    {
        AnalyticsItem newItem = new AnalyticsItem();
        newItem.id = userID;
        newItem.timestamp = _timestamp.ToString(DATETIME_FORMAT);
        newItem.objectName = _objectName;
        newItem.interaction = _interaction;
        //newItem.name = name;
        //newItem.value = value;
        currentSession.items.Add(newItem);
    }

    //WRITE FILE
    private void Serialize<T>(List<AnalyticsSession> list)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
        StreamWriter tw = new StreamWriter(Application.dataPath + "/" + FILE_NAME);
        serializer.Serialize(tw, list);
        tw.Close();

        if (debug)
            Debug.Log("<b><color=#ff3da8> " + list.Count + "</color></b> <color=#ff0379> total analytics sessions saved at: " + DateTime.Now.ToString(DATETIME_FORMAT) + "</color>");

        sessions.Clear();
        currentSession.items.Clear();
    }

    //READ FILE
    public List<T> GenericDeSerialize<T>()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
        StreamReader tr = new StreamReader(Application.dataPath + "/" + FILE_NAME);
        if (tr.ReadLine() == null)
            return new List<T>();
        List<T> b = (List<T>)serializer.Deserialize(tr);
        tr.Close();
        return b;
    }

    private bool ValidateFile()
    {

        if (File.Exists(Application.dataPath + "/" + FILE_NAME))
        {
            return true;
        }
        File.CreateText(Application.dataPath + "/" + FILE_NAME);
        return false;
    }
}