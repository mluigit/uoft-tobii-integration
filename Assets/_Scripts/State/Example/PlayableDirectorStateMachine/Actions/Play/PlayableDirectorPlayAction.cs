﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

/// <summary>
/// This is called by the OnEnter event of a State scriptable object.
/// It will notify the state machine that called the state to play a timeline.
/// The logic behind selecting which timeline asset to play is handled by the state machine.
/// </summary>

[CreateAssetMenu(menuName = "Playable Director State Machine/Actions/Play")]
public class PlayableDirectorPlayAction : StateAction {

    public TimelineAsset timelineAsset;
    public DirectorWrapMode wrapMode = DirectorWrapMode.None;

    public override void Act(StateMachine stateMachine)
    {

        PlayableDirectorStateMachine directorStateMachine = stateMachine as PlayableDirectorStateMachine;
        directorStateMachine.Play(timelineAsset, wrapMode);
    }
}
