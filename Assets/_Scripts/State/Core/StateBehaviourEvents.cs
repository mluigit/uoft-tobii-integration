﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AXSCore
{
    public class StateBehaviourEvents : StateBehaviour
    {

        public UnityEvent onStateEnterEvents;
        public UnityEvent onStateExitEvents;

        public override void OnActiveStateEnter(StateMachine _stateMachine, State _state)
        {
            base.OnActiveStateEnter(_stateMachine, _state);
            onStateEnterEvents.Invoke();
        }

        public override void OnActiveStateExit(StateMachine _stateMachine, State _state)
        {
            base.OnActiveStateExit(_stateMachine, _state);
            onStateExitEvents.Invoke();
        }
    }
}
