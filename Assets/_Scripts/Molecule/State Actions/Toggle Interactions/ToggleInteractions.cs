﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Actions/Toggle Interaction")]
public class ToggleInteractions : StateAction {

    public bool enableInteractions;

    public override void Act(StateMachine stateMachine)
    {
        Collider collider = stateMachine.GetComponentInChildren<Collider>();
        if (collider != null)
            collider.enabled = enableInteractions;
    }
}
