﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Actions/Hover Active Site")]
public class HoverActiveSiteAction : StateAction {

    public override void Act(StateMachine stateMachine)
    {
        MoleculeStateMachine moleculeStateMachine = stateMachine as MoleculeStateMachine;

        if (moleculeStateMachine == null)
            return;

        //do nothing if this molecule is not attached to a hand
        if (moleculeStateMachine.attachedHand == null)
        {
            return;
        }
            

        //get the pointer state machine associated with this object's attached hand
        PointerStateMachine pointerStateMachine = moleculeStateMachine.attachedHand.GetComponent<PointerStateMachine>();

        //reset hand
        moleculeStateMachine.RemoveAttachedHand(moleculeStateMachine.attachedHand);

        if (pointerStateMachine == null)
            return;

        //check if we are hovered over something
        if (pointerStateMachine.currentState != pointerStateMachine.hoverInState)
            return;

        //get ligand
        MoleculeStateMachine target = pointerStateMachine.PointerEventArgs.target.GetComponent<MoleculeStateMachine>();
        if (target == null)
            return;

        //move this object to the active site of the target
        if(target.molecule.ligandMols.Contains(moleculeStateMachine.molecule))
            target.MoveToActiveSite(moleculeStateMachine);
    }
}
