﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Molecule/Actions/Unparent")]
public class UnparentAction : StateAction {

    public override void Act(StateMachine stateMachine)
    {
        stateMachine.transform.parent = null;
    }
}
