﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/State Action/Random Motion")]
public class RandomMotion : StateAction {

    public bool start;

    public override void Act(StateMachine stateMachine)
    {
        MoleculeStateMachine moleculeStateMachine = stateMachine as MoleculeStateMachine;
        if (moleculeStateMachine == null)
            return;

        moleculeStateMachine.RandomMotion(start);
    }
}
