﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoleculeManager : MonoBehaviour {

    private static MoleculeManager _instance;
    public static MoleculeManager Instance { get { return _instance; } }


    private List<MoleculeStateMachine> _moleculeStateMachines;
    public List<MoleculeStateMachine> MoleculeStateMachines { get { return _moleculeStateMachines; } }

    [System.Serializable]
    public class MoleculeInfo
    {
        public Molecule molecule;
        public int max;
        public int count;

        public MoleculeInfo(Molecule _mol, int _max)
        {
            molecule = _mol;
            max = _max;
        }
    }

    public GameObject moleculePrefab;
    public State freeState;
    public State bindingState;
    public State targetState;
    public State processingState;
    public Molecule operatorSite;
    public Molecule capSite;
    [Header("Inducers")]
    public Molecule glucose;
    public Molecule lactose;
    public Molecule camp;
    public Molecule crp_free;
    public Molecule crp_bound;

    [Header("Molecule Count Preset")]
    public MoleculeCountPreset preset;

    private bool _repressed;
    public bool Repressed { get { return _repressed; } set { _repressed = value; } }

    private bool _cap;
    public bool Cap { get { return _cap; } set { _cap = value; } }

    private bool _transcribe;
    public bool Transcribe { get { return _transcribe; } set { _transcribe = value; } }


    public Molecule[] molecules;
    public List<MoleculeInfo> moleculeInfo;

    private void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Destroy(_instance.gameObject);
        }
        else
        {
            _instance = this;
        }

        _moleculeStateMachines = new List<MoleculeStateMachine>();

        //generate a list of molecule values
        moleculeInfo = new List<MoleculeInfo>();
        foreach(Molecule molecule in molecules)
        {
            if (molecule == null)
                continue;
            MoleculeInfo newInfo = new MoleculeInfo(molecule, molecule.maxNumber);
            if(!moleculeInfo.Contains(newInfo))
                moleculeInfo.Add(newInfo);
        }
    }

    //add a molecule to the list
    public void AddMolecule(MoleculeStateMachine mol)
    {
        _moleculeStateMachines.Add(mol);
        UpdateCount(mol.molecule, 1);
    }

    //remove a molecule from the list
    public void RemoveMolecule(MoleculeStateMachine mol)
    {
        if (_moleculeStateMachines.Contains(mol))
        {
            _moleculeStateMachines.Remove(mol);
            UpdateCount(mol.molecule, -1);
        }
    }

    private void UpdateCount(Molecule mol, int amount)
    {
        foreach(MoleculeInfo info in moleculeInfo)
        {
            if(info.molecule == mol)
            {
                info.count += amount;
            }
        }
    }

    //get a list of moleculestatemachines with molecule
    public List<MoleculeStateMachine> GetMoleculesOfType(Molecule molecule)
    {
        List<MoleculeStateMachine> molList = new List<MoleculeStateMachine>();
        foreach(MoleculeStateMachine m in _moleculeStateMachines)
        {
            if (m.molecule == molecule)
                molList.Add(m);
        }
        return molList;
    }

    //get the number of molecules of a particular type
    public int GetNumberOfMolecules(Molecule molecule)
    {
        return GetMoleculesOfType(molecule).Count;
    }

    //have we maxed out this molecule?
    public bool AtMax(Molecule mol)
    {
        foreach(MoleculeInfo info in moleculeInfo)
        {
            if(info.molecule == mol)
            {
                if (info.count >= info.max)
                    return true;
                else
                    return false;
            }
        }
        return false;
    }

    public MoleculeStateMachine FindFreeSceneMolecule(Molecule mol)
    {
        List<MoleculeStateMachine> molStateMachines = GetMoleculesOfType(mol);
        foreach(MoleculeStateMachine m in molStateMachines)
        {
            if (m.currentState == freeState)
                return m;
        }
        return null;
    }

    //Spwan Molecule
    public MoleculeStateMachine SpawnMolecule(Molecule molecule, Transform trans)
    {
        //check if we're at the max and destroy free duplicates
        if (AtMax(molecule) && molecule.pool && FindFreeSceneMolecule(molecule) != null)
            Destroy(FindFreeSceneMolecule(molecule).gameObject);

        GameObject newMolecule;

        if(trans == null)
        {
            Vector3 randomPosition = UnityEngine.Random.insideUnitSphere * 0.5f + transform.position;
            newMolecule = Instantiate(moleculePrefab, randomPosition, Quaternion.identity);
        }
        else
        {
            newMolecule = Instantiate(moleculePrefab, trans.position, trans.rotation);
        }
        
        MoleculeStateMachine newMolStateMachine = newMolecule.GetComponent<MoleculeStateMachine>();
        newMolStateMachine.SetMolecule(molecule);

        return newMolStateMachine;
    }

    IEnumerator spawnSequence;
    public void SpawnContinuous(Molecule molecule, Transform trans, float interval)
    {
        /*
        if (spawnSequence != null)
            StopCoroutine(spawnSequence);
        spawnSequence = C_SpawnContinuous(molecule, trans, interval);
        StartCoroutine(spawnSequence);
        */
        StartCoroutine(C_SpawnContinuous(molecule, trans, interval));
    }

    IEnumerator C_SpawnContinuous(Molecule molecule, Transform trans, float interval)
    {
        yield return new WaitForSeconds(interval);
        SpawnMolecule(molecule, trans);
        SpawnContinuous(molecule, trans, interval);
    }

    public void ResetOperator()
    {
        List<MoleculeStateMachine> opStateMachine = GetMoleculesOfType(operatorSite);
        foreach(MoleculeStateMachine m in opStateMachine)
        {
            m.ResetMolecule();
        }
        MoleculeManager.Instance.Repressed = false;
    }

    public void RestCapSite()
    {
        List<MoleculeStateMachine> opStateMachine = GetMoleculesOfType(capSite);
        foreach (MoleculeStateMachine m in opStateMachine)
        {
            m.ResetMolecule();
        }
        MoleculeManager.Instance.Cap = false;
    }

    #region GLUCOSE SPAWNING
    //add glucose to the scene
    public void AddGlucose(Transform trans = null)
    {
        SpawnMolecule(glucose, trans);
        RemoveCamp();
    }

    //remove glucose from the scene
    public void RemoveGlucose()
    {
        MoleculeStateMachine glucoseStateMachine = FindFreeSceneMolecule(glucose);
        if (glucoseStateMachine == null)
            return;

        Destroy(glucoseStateMachine.gameObject);
        AddCamp();
    }

    #endregion

    #region LACTOSE SPAWNING
    public void AddLactose(Transform trans = null)
    {
        SpawnMolecule(lactose, trans);
    }

    public void RemoveLactose()
    {
        MoleculeStateMachine lactoseStateMachine = FindFreeSceneMolecule(lactose);
        if (lactoseStateMachine != null)
            Destroy(lactoseStateMachine.gameObject);
    }
    #endregion

    #region cAMP SPAWNING
    //add glucose to the scene
    public void AddCamp(Transform trans = null)
    {
        SpawnMolecule(camp, trans);
    }

    //remove glucose from the scene
    public void RemoveCamp()
    {
        MoleculeStateMachine stateMachine = FindFreeSceneMolecule(camp);
        if (stateMachine == null)
        {
            stateMachine = FindFreeSceneMolecule(crp_bound);
            if (stateMachine != null)
            {
                Destroy(stateMachine.gameObject);
                SpawnMolecule(crp_free, null);
                SpawnMolecule(camp, null);
            }      
        } 
        else
            Destroy(stateMachine.gameObject);  
    }

    #endregion

    public void SetMoleculeCountPreset(MoleculeCountPreset preset)
    {
        //reset cap site
        RestCapSite();

        //remove all glucose
        List<MoleculeStateMachine> glucoseList = GetMoleculesOfType(glucose);
        foreach (MoleculeStateMachine m in glucoseList)
        {
            Destroy(m.gameObject);
        }

        //remove all camp
        List<MoleculeStateMachine> campList = GetMoleculesOfType(camp);
        foreach (MoleculeStateMachine m in campList)
        {
            Destroy(m.gameObject);
        }

        //remove all lactose
        List<MoleculeStateMachine> lactoseList = GetMoleculesOfType(lactose);
        foreach (MoleculeStateMachine m in lactoseList)
        {
            Destroy(m.gameObject);
        }

        //destroy bound camp
        List<MoleculeStateMachine> crpBoundList = GetMoleculesOfType(crp_bound);
        foreach(MoleculeStateMachine m in crpBoundList)
        {
            SpawnMolecule(crp_free, m.transform); //replace with free camp
            Destroy(m.gameObject);
        }

        //reset free camp
        List<MoleculeStateMachine> campBoundList = GetMoleculesOfType(crp_free);
        foreach (MoleculeStateMachine m in campBoundList)
        {
            m.ResetMolecule();
        }

        //spawn glucose
        for(int i = 0; i < preset.glucoseCount; i++)
        {
            SpawnMolecule(glucose, null);
        }

        //spawn inverse amount of camp
        int campCount = (int) Mathf.Max(glucose.maxNumber - preset.glucoseCount, 0f);
        if(campCount > 0)
        {
            for (int i = 0; i < campCount; i++)
            {
                SpawnMolecule(camp, null);
            }
        }

        //spawn lactose
        for (int i = 0; i < preset.lactoseCount; i++)
        {
            SpawnMolecule(lactose, null);
        }

    }
}
