﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/State Action/Auto Bind")]
public class AutoBindAction : StateAction {

    public bool autoBind;

    public override void Act(StateMachine stateMachine)
    {
        MoleculeStateMachine moleculeStateMachine = stateMachine as MoleculeStateMachine;
        if (moleculeStateMachine == null)
            return;

        moleculeStateMachine.AutoBind = autoBind;
    }
}
