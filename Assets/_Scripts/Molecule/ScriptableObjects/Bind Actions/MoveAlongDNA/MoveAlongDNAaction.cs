﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Move Along DNA")]
public class MoveAlongDNAaction : BindAction {

    public WaypointManager.WaypointGroup group;
    [Range(0f,1f)]
    public float bindProbability = 0.5f;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        RNApolymeraseController controller = boundMolecule.GetComponent<RNApolymeraseController>();

        if (controller == null)
            return;

        //check is LAC is being repressed
        if (group == WaypointManager.WaypointGroup.LAC_FULL && MoleculeManager.Instance.Repressed)
        {
            controller.ForceReset();
            return;
        }
        else if (group == WaypointManager.WaypointGroup.LAC_FULL && !MoleculeManager.Instance.Cap)
        {
            float rand = Random.value;
            {
                if (rand <= bindProbability)
                {
                    MoleculeManager.Instance.Transcribe = true;
                }

                else
                {
                    MoleculeManager.Instance.Transcribe = false;
                    controller.ForceReset();
                    return;
                }

            }
        }

        controller.SetGroup(group);
        controller.StartMoving();
    }
}
