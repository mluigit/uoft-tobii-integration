﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Grow RNA")]
public class GrowRNAaction : BindAction {

    public Molecule mRNAtype;
    public WaypointManager.WaypointGroup group;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        //check is LAC is being repressed
        if (group == WaypointManager.WaypointGroup.LAC_FULL && MoleculeManager.Instance.Repressed)
        {
            return;
        }

        mRNAObjectPooler pooler = FindObjectOfType<mRNAObjectPooler>();

        pooler.SpawnmRNA(boundMolecule, boundMolecule, mRNAtype);
    }
}
