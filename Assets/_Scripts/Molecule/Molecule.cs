﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/New Molecule")]
public class Molecule : ScriptableObject {

    public string name;
    public LayerMask layerMask;
    public GameObject model;
    public GameObject tobiiModel;
    public List<Molecule> ligandMols;
    public BindAction[] bindActions;
    public StateAction[] spawnActions;
    public float radius;
    public bool contrainTransform;
    public float cooldownTime;
    public int maxNumber;
    public bool pool = true;
}
