﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using TobiiProVRAnalytics;
using System;

public class MoleculeStateMachine : StateMachine {

    #region PUBLIC VARIABLES

    //molecule scriptable object
    public Molecule molecule;
    public State freeState;

    //label
    public LabelController label; //TODO: target canvas group to tween alpha in and out

    //model holder
    public Transform modelParent;

    //hovered variables
    private Hand _attachedHand;
    public Hand attachedHand { get { return _attachedHand; } }
    

    //radius
    public float Raius { get { return _radius; } set { _radius = value; } }
    public float _radius;
   

    [System.Serializable]
    public class ActiveSite
    {
        public Transform site;
        public bool bound;

        public ActiveSite(Transform newSite)
        {
            site = newSite;
            bound = false;
        }
    }

    //Transforms to which other molecules may bind
    public List<ActiveSite> activeSites;

    //Hover
    private bool _hovered;
    public bool Hovered { get { return _hovered; } set { _hovered = value; } }

    //Attachment
    private bool _attached;
    public bool Attached { get { return _attached; } set { _attached = value; } }

    //Binding
    private bool _binding;
    public bool Binding { get { return _binding; } set { _binding = value; } }

    private bool _bound;
    public bool Bound { get { return _bound; } set { _bound = value; } }

    private bool _autoBind;
    public bool AutoBind { get { return _autoBind; } set { _autoBind = value; } }

    #endregion

    #region PRIVATE VARIABLES

    //cached Transform
    private Transform _transform;

    //interactable
    private Interactable _interactable;

    #endregion

    #region INITIALIZATION

    private void Start()
    {
        _transform = transform;
        _interactable = GetComponent<Interactable>();
        

        if (molecule == null)
            return;

        //spawn the model if it's not already present in the scene
        if (modelParent.childCount < 1)
            SetMolecule(molecule);
        else
        {
            label.SetLabel(molecule.name); //...or just update the label
            MoleculeManager.Instance.AddMolecule(this);
            _radius = molecule.radius;
        }
            
    }

    protected override void OnStateMachineEnabled()
    {
        base.OnStateMachineEnabled();
        _interactable = GetComponent<Interactable>();
        if (_interactable != null)
        {
            _interactable.onAttachedToHand += SetAttachedHand;
            //_interactable.onDetachedFromHand += RemoveAttachedHand;
        }
    }

    private void OnDisable()
    {
        if (_interactable != null)
        {
            _interactable.onAttachedToHand -= SetAttachedHand;
            //_interactable.onDetachedFromHand -= RemoveAttachedHand;
        }

        MoleculeManager.Instance.RemoveMolecule(this);
    }

    private void OnDestroy()
    {
        MoleculeManager.Instance.RemoveMolecule(this);
    }

    #endregion

    #region LIGAND BINDING

    public void IsLigandInRange()
    {

        //don't bother casting if this molecule has no ligand
        if (molecule.ligandMols.Count < 1)
            return;

        //don't bother casting if active sites are not available
        if (AllAvailableSites().Count < 1)
            return;

        //does a ligand fall within an overlap sphere?
        Collider[] hitColliders = Physics.OverlapSphere(_transform.position, _radius);
        foreach(Collider c in hitColliders)
        {
            if(c.GetComponentInParent<MoleculeStateMachine>() != null)
            {
                //check if detected molecule is this molecule's ligand
                MoleculeStateMachine mol = c.GetComponentInParent<MoleculeStateMachine>();
                if(IsLigand(mol))
                {
                    //Debug.Log("Ligand found: " + mol.molecule.name);
                }
            }
        }
    }

    //returns true if specified statemachine's molecul is this molecule's ligand
    private bool IsLigand(MoleculeStateMachine moleculeStateMachine)
    {
        //do nothing if molecule is not free
        if (moleculeStateMachine.currentState != MoleculeManager.Instance.freeState)
            return false;

        if (molecule.ligandMols.Contains(moleculeStateMachine.molecule))
        {
            //start binding process on ligand
            moleculeStateMachine.ChangeState(MoleculeManager.Instance.bindingState);
            MoveToActiveSite(moleculeStateMachine);

            return true;
        }
        return false;
    }

    public void MoveToActiveSite(MoleculeStateMachine moleculeStateMachine)
    {
        ChangeState(MoleculeManager.Instance.processingState);

        //notify ligand it is binding
        moleculeStateMachine.Binding = true;
                
        //find and active site to bind to
        ActiveSite activeSite = ClosestAvailableSite(moleculeStateMachine.transform);
        if(activeSite != null)
        {
            activeSite.bound = true; //flag active site as occupied
            StartCoroutine(C_MoveToActiveSite(moleculeStateMachine, activeSite));
        }
    }

    IEnumerator C_MoveToActiveSite(MoleculeStateMachine moleculeStateMachine, ActiveSite activeSite)
    {
        Transform ligand= moleculeStateMachine.transform;
        Transform site = activeSite.site;

        while (ligand != null && site != null && Vector3.Distance(ligand.position, site.position) > 0.05f)
        {
            ligand.position = Vector3.Lerp(ligand.position, site.position, Time.fixedDeltaTime / Vector3.Distance(ligand.position, site.position));
            ligand.rotation = Quaternion.Lerp(ligand.rotation, site.rotation, Time.fixedDeltaTime / Vector3.Distance(ligand.position, site.position));
            yield return null;
        }

        if(ligand != null)
        {
            ligand.SetParent(site, true);
            ligand.localPosition = Vector3.zero;
            ligand.localRotation = Quaternion.identity;
        }
        

        //trigger bind actions
        if(FirstAvailableActiveSite() == null)
            BindingActions(moleculeStateMachine, site);
    }

    //returns any active site that does not have a bound ligand or ligand in transit to the site
    private ActiveSite FirstAvailableActiveSite()
    {
        foreach (ActiveSite a in activeSites)
        {
            if (!a.bound)
                return a;
        }
        return null;
    }

    private ActiveSite ClosestAvailableSite(Transform ligand)
    {
        //no sites available
        if (AllAvailableSites().Count < 1)
            return null;

        float tempDistance = Mathf.Infinity;
        ActiveSite tempSite = null;
        foreach(ActiveSite a in AllAvailableSites())
        {
            if (a.site == null)
                continue;
            float distance = Vector3.Distance(a.site.position, ligand.position);
            if (distance < tempDistance)
            {
                tempSite = a;
                tempDistance = distance;
            }
        }
        return tempSite;
    }

    //returns true if all active site are occupied
    private List<ActiveSite> AllAvailableSites()
    {
        List<ActiveSite> availableSites = new List<ActiveSite>();
        foreach (ActiveSite a in activeSites)
        {
            if (a.site == null)
                continue;
            if (!a.bound)
                availableSites.Add(a);
        }
        return availableSites;
    }

    #endregion

    #region HAND ATTACHMENT

    public void SetAttachedHand(Hand hand)
    {
        _attachedHand = hand;
    }

    public void RemoveAttachedHand(Hand hand)
    {
        _attachedHand = null;
    }

    #endregion

    #region LABELS

    public void ToggleLabel(bool show)
    {
        label.ToggleLabel(show);
    }

    public void ToggleLabel(bool show, Vector3 worldPos)
    {
        ToggleLabel(show);
        label.transform.position = worldPos;
    }

    #endregion

    #region BINDING ACTIONS
    private void BindingActions(MoleculeStateMachine boundMol, Transform site)
    {
        foreach(BindAction action in molecule.bindActions)
        {
            if(action != null)
                action.Act(this, boundMol, site);
        }

        ChangeState(MoleculeManager.Instance.targetState);
    }
    #endregion

    #region SPAWN ACTIONS
    public void SpawnActions()
    {
        if (molecule == null)
            return;

        foreach(StateAction action in molecule.spawnActions)
        {
            if (action != null)
                action.Act(this);
        }

        ChangeState(MoleculeManager.Instance.freeState);
    }

    #endregion

    #region SPAWN MOLECULE

    //change molecule to different type and update model
    public void SetMolecule(Molecule mol)
    {
        //clear active sites
        activeSites = new List<ActiveSite>();

        //set radius
        _radius = mol.radius;

        //set autoBind
        _autoBind = true;

        //remove old model
        if (molecule != null && modelParent.childCount > 0)
        {
            foreach(Transform child in modelParent)
            {
                Destroy(child.gameObject);
            }

            //update molecule manager
             MoleculeManager.Instance.RemoveMolecule(this);
        }

        GameObject newModel = Instantiate(mol.model, modelParent.position, modelParent.rotation, modelParent);

        if (TobiiProVRAnalyticsManager.Instance != null)
        {
            //register Tobii object to manager
            //Debug.Log("trackedObject: " + this.gameObject.name);
            TobiiProVRAnalytics_TrackedObject trackedObject;
            if (this.gameObject.GetComponent<TobiiProVRAnalytics_TrackedObject>() != null)
            {
                trackedObject = this.gameObject.GetComponent<TobiiProVRAnalytics_TrackedObject>();
            }
            else
            {
                trackedObject = this.gameObject.AddComponent<TobiiProVRAnalytics_TrackedObject>();
            }
            
            this.gameObject.name = mol.name + " " + uniqueID();
            trackedObject.ResetComponent();
            AoIManager.Instance.ObjectsByInstanceId.Add(trackedObject.instanceId, trackedObject);
            AoIManager.Instance.ProductNames.Add(trackedObject.trackedObjectName);
        }

        //set active sites
        UpdateActiveSites(newModel.transform);

        /*
        if(TobiiProVRAnalyticsManager.Instance != null)
        {
            GameObject newModel = Instantiate(mol.tobiiModel, modelParent.position, modelParent.rotation, modelParent);

            //register Tobii object to manager
            TobiiProVRAnalytics_TrackedObject trackedObject = newModel.GetComponentInChildren<TobiiProVRAnalytics_TrackedObject>();
            if(trackedObject != null)
            {
                AoIManager.Instance.ObjectsByInstanceId.Add(trackedObject.instanceId, trackedObject);
                AoIManager.Instance.ProductNames.Add(trackedObject.trackedObjectName);
            }

            //set active sites
            UpdateActiveSites(newModel.transform);
        }
        else
        {
            GameObject newModel = Instantiate(mol.model, modelParent.position, modelParent.rotation, modelParent);
            //set active sites
            UpdateActiveSites(newModel.transform);
        }
        */
        //update molecule manager
        molecule = mol;
        MoleculeManager.Instance.AddMolecule(this);

        //set label text
        label.SetLabel(molecule.name);

        //call spawn actions
        SpawnActions();
        /*
        //constrain?
        if(molecule.contrainTransform)
        {
            Rigidbody rigidbody = GetComponent<Rigidbody>();
            if(rigidbody != null)
            {
                rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            }
        }
        else
        {
            Rigidbody rigidbody = GetComponent<Rigidbody>();
            if (rigidbody != null)
            {
                rigidbody.constraints = RigidbodyConstraints.None;
            }
        }
        */

    }

    IEnumerator WaitForAoI(TobiiProVRAnalytics_TrackedObject trackedObject)
    {
        while(AoIManager.Instance == null || AoIManager.Instance.ObjectsByInstanceId == null || AoIManager.Instance.ProductNames == null)
        {
            yield return new WaitForEndOfFrame();
        }


    }

    private void UpdateActiveSites(Transform model)
    {
        ActiveSiteTransform[] activeSiteTransforms = model.GetComponentsInChildren<ActiveSiteTransform>();
        
        foreach(ActiveSiteTransform a in activeSiteTransforms)
        {
            activeSites.Add(new ActiveSite(a.transform));
        }
    }
    #endregion

    #region    UTILS
    //force molecule into free state
    public void FreeMolecule()
    {
        ChangeState(freeState);
    }

    public void ResetMolecule()
    {
        _binding = false;
        ChangeState(MoleculeManager.Instance.freeState);
        foreach(ActiveSite a in activeSites)
        {
            a.bound = false;
        }
    }

    public void ResetMolecule(float delay)
    {
        StartCoroutine(C_ResetMolecule(delay));
    }

    IEnumerator C_ResetMolecule(float delay)
    {
        yield return new WaitForSeconds(delay);
        ResetMolecule();
    }

    IEnumerator expandSequence;
    public void ExpandRadius(float delay)
    {
        if (!_autoBind)
            return;

        _radius = molecule.radius;
        if (expandSequence != null)
            StopCoroutine(expandSequence);
        expandSequence = C_ExpandRadius(delay);
        StartCoroutine(expandSequence);
    }

    
    IEnumerator C_ExpandRadius(float delay)
    {
        yield return new WaitForSeconds(delay);
        while(!_binding)
        {
            _radius += Time.fixedDeltaTime;
            yield return new WaitForEndOfFrame();
            if (_radius >= 5f)
                _radius = molecule.radius;
        }
    }

    public void StopExpandingRadius()
    {
        _radius = molecule.radius;
        if (expandSequence != null)
            StopCoroutine(expandSequence);
    }

    string uniqueID()
    {
        DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        int currentEpochTime = (int)(DateTime.UtcNow - epochStart).TotalSeconds;
        int z1 = UnityEngine.Random.Range(0, 1000000);
        int z2 = UnityEngine.Random.Range(0, 1000000);
        string uid = currentEpochTime + ":" + z1 + ":" + z2;
        return uid;
    }
    #endregion

    #region RANDOM MOTION
    IEnumerator motionSequence;
    public void RandomMotion(bool start)
    {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = UnityEngine.Random.onUnitSphere * 2f;
        /*
        if (motionSequence != null)
            StopCoroutine(motionSequence);
        if(start)
        {
            Rigidbody rigidbody = GetComponent<Rigidbody>();
            motionSequence = C_RandomMotion(rigidbody);
            StartCoroutine(motionSequence);
        }
        */
    }

    IEnumerator C_RandomMotion(Rigidbody rigidbody)
    {
        rigidbody.velocity = UnityEngine.Random.onUnitSphere * 0.5f;
        yield return new WaitForSeconds(5f);
        RandomMotion(true);
    }
    #endregion

    
}
