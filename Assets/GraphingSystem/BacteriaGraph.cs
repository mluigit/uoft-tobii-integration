﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BacteriaGraph : MonoBehaviour
{

    public WindowGraph graph;

    [SerializeField] private int glucoseTotal;
    private int lastGluTotal;
    [SerializeField] private int lactoseTotal;
    private int lastLactose;
    public int waitTime;
    public int initialLagTime;
    public int stationaryTime;

    private float timeCheck;
    private float lastTime;

    private bool isDying;
    private bool lagComplete;
    private bool islactoseLag;
    void Start()
    {
        glucoseTotal = 0;
        lactoseTotal = 0;
        lastGluTotal = glucoseTotal;
        lastLactose = lactoseTotal;

        isDying = false;
        lagComplete = false;
        islactoseLag = false; 

        StartCoroutine(UpdateLevels());

    }

    public void AddGlucose(int add)
    {
        timeCheck = lastTime = 0f;

        lastGluTotal = glucoseTotal;
        glucoseTotal += add;
        if (glucoseTotal < 0)
        {
            glucoseTotal = 0;
        }

        if (glucoseTotal > 0 && glucoseTotal > lastGluTotal)
        {
            isDying = false;
        }

    }

    public void AddLactose(int add)
    {
        lastLactose = lactoseTotal;
        lactoseTotal += add;

        if(lactoseTotal < 0)
        {
            lactoseTotal = 0;
        }

        if (lactoseTotal > 0 && lactoseTotal > lastLactose)
        {
            isDying = false;
        }
    }

    public int getGlucose()
    {
        return glucoseTotal;
    }

    public int getLactose()
    {
        return lactoseTotal;
    }
    private IEnumerator UpdateLevels() {
        int state = 6;
        while (true)
        {
            switch (state)
            {
                case 0: //initial lag state
                    yield return new WaitForSeconds(initialLagTime);
                    graph.AddTotal(0, initialLagTime);
                    print("initial state");
                    state = 4;
                    break;
                case 1: //stationary glucose state 
                    //if (glucoseTotal == 0 && lastGluTotal > glucoseTotal & !isDying)
                    //{
                        yield return new WaitForSeconds(stationaryTime);
                        graph.AddTotal(0, stationaryTime);
                        isDying = true;
                        state = 3;
                    //}
                        break;
                case 2: //stationary lactose state
                    break;
                case 3: //dying state
                    if (lactoseTotal >= 1)
                    {
                        state = 5;
                        break;
                    }
                    if (graph.GetTotal() == 0) //stop the graph once graph hits zero (completely dead)
                    {
                        yield return new WaitForSeconds(waitTime);
                    }
                    else
                    {
                        yield return new WaitForSeconds(waitTime);
                        graph.AddTotal(-2, waitTime);
                        print("dying state, " + timeCheck);
                    }
                    break;
                case 4: //glucose increase state
                    if (glucoseTotal >= 1)
                    {
                        yield return new WaitForSeconds(waitTime);
                        timeCheck = Time.time - timeCheck;
                        graph.AddTotal(2, waitTime);
                    }
                    else
                    {
                        state = 1;
                    }
                    break;
                case 5: //lactose increase state
                    if (lactoseTotal >= 1)
                    {
                        yield return new WaitForSeconds(waitTime);
                        timeCheck = Time.time - timeCheck;
                        graph.AddTotal(2, waitTime);
                    }
                    else
                    {
                        state = 1;
                    }
                    break;
                case 6:
                    yield return new WaitForSeconds(waitTime);
                    print("waiting state");
                    if (glucoseTotal >= 1) {
                        state = 0;
                    }
                    break;
            }

        }
    }
    private IEnumerator UpdateGlucoseLevel()
    {
        while (true)
        {
            if (glucoseTotal == 1 && !lagComplete) //initial lag state
            {
                yield return new WaitForSeconds(initialLagTime);
                graph.AddTotal(0, initialLagTime);
                lagComplete = true;

                print("initial state");
            }
            else if (((glucoseTotal == 0 && lastGluTotal > glucoseTotal) || (lactoseTotal == 0 && lastLactose > lactoseTotal)) && !isDying) //stationary state
            {
                if (lagComplete)
                {
                    lagComplete = false;
                }

                //yield return new WaitForSeconds(stationaryTime);

                //graph.AddTotal(0, stationaryTime);

                if (lactoseTotal >= 1)
                {
                    if (lastLactose == 0 && lactoseTotal == 1)
                    {
                        yield return new WaitForSeconds(stationaryTime);
                        graph.AddTotal(0, stationaryTime);
                        lastLactose = 1;
                    }
                    yield return new WaitForSeconds(waitTime);
                    graph.AddTotal(2, waitTime);
                }
                else
                {
                    yield return new WaitForSeconds(stationaryTime);
                    graph.AddTotal(0, stationaryTime);
                    isDying = true;
                }
                print("stationary state, " + timeCheck);

            }
            else if (glucoseTotal == 0 && lactoseTotal == 0 && isDying) //dying state
            {

                if (graph.GetTotal() == 0) //stop the graph once graph hits zero (completely dead)
                {
                    yield return new WaitForSeconds(waitTime);
                }
                else
                {
                    //lastTime = timeCheck;
                    //timeCheck = Time.time;
                    yield return new WaitForSeconds(waitTime);
                    //timeCheck = Time.time - timeCheck;
                    //if (lastTime > timeCheck)
                    //{
                    //    timeCheck += lastTime;
                    //}

                    graph.AddTotal(-2, waitTime);

                    print("dying state, " + timeCheck);
                }

            }
            else if (glucoseTotal == 0 && lactoseTotal>= 1)
            {
                if (lastLactose == 0 && lactoseTotal == 1)
                {
                    yield return new WaitForSeconds(stationaryTime);
                    graph.AddTotal(0, stationaryTime);
                    lastLactose = 1;
                }
                yield return new WaitForSeconds(waitTime);
                graph.AddTotal(2, waitTime);
            }
            else if (glucoseTotal >= 1) //exponentially increasing state
            {

                yield return new WaitForSeconds(waitTime);
                timeCheck = Time.time - timeCheck;
                graph.AddTotal(2, waitTime);

                print("increase state, " + timeCheck);
            }
            else
            {
                yield return new WaitForSeconds(waitTime);

                print("waiting state");
            }
        }
    }

}

