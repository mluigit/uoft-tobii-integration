using System;
using System.Collections.Generic;
using UnityEngine;

public class TempLicenseCheck : MonoBehaviour {

    public GameObject licensePopup;

    void Awake()
    {
        if (DateTime.Now > new DateTime(2018, 12, 31))
        {
            licensePopup.gameObject.SetActive(true);
        }
    }
}
