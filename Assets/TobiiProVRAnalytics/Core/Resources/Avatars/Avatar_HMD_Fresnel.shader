﻿Shader "Custom/Avatar_HMD_Fresnel" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_RefIntensity("Rim Intensity", Range(0,5)) = 0
		_rimPower("Rim Power", Range(0,10)) = 0
		_rimOffset("Rim Offset", Range(0,10)) = 0

	}
		SubShader{
		Tags{ "RenderType" = "Transparent" } //change to transparent
		LOD 400



		CGPROGRAM
#pragma surface surf Lambert alpha
#pragma vertex vert
#pragma target 3.0


		struct Input {
		float2 uv_MainTex;
		float3 worldNormal;
		float3 worldRefl;
		float3 viewDir;
		INTERNAL_DATA
	};

	half4 _Color;
	half _RefIntensity;
	half _rimPower;
	fixed _rimOffset;


	void vert(inout appdata_full v, out Input o) {
		UNITY_INITIALIZE_OUTPUT(Input, o);
		o.worldNormal = v.vertex.xyz;
	}


	void surf(Input IN, inout SurfaceOutput o) {


		float3 worldRefl = WorldReflectionVector(IN, o.Normal);
		half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
		half fRim = pow(rim, _rimPower) * _RefIntensity;

		o.Emission = fRim * _Color;
		o.Alpha = fRim + _rimOffset;

	}
	ENDCG
	}
		FallBack "Specular"
}

