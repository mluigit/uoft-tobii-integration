﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TobiiProVRAnalytics;

public class TobiiTrackedObjectGenerator : MonoBehaviour {

    private void OnEnable()   
    {
        TobiiProVRAnalytics_TrackedObject trackedObject = this.gameObject.AddComponent<TobiiProVRAnalytics_TrackedObject>();
        trackedObject.ResetComponent();
        AoIManager.Instance.ObjectsByInstanceId.Add(trackedObject.instanceId, trackedObject);
    }
    //private void Update()
    //{
    //    GameObject[] molecules = FindObjectsOfType<GameObject>();
    //    for (int i = 0; i < molecules.Length; i++)
    //    {
    //        if (molecules[i].layer == 9)
    //        {
    //            if (molecules[i].GetComponent<TobiiProVRAnalytics_TrackedObject>() == null) { }
    //            //molecules[i].AddComponent<TobiiProVRAnalytics_TrackedObject>();
    //        }
    //    }
    //}
}
